import java.util.Random;

/**
 * 18.02.2021
 * 11. HumanTask
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Generator {
    //Creator of channels
    public Channel[] generateC(int count) {
        String names[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"};
        int numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        Random random = new Random();
        Channel channels[] = new Channel[count];

        for (int i = 0; i < channels.length; i++) {
            channels[i] = new Channel(names[random.nextInt(names.length)] + names[random.nextInt(names.length)], i+1);
        }

        return channels;
    }
    //Creator of programs
    public Program[] generateP(int count) {
        String names[] = {"L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "BB", "CC", "DD", "EE"};

        Random random = new Random();
        Program programs[] = new Program[count];

        for (int i = 0; i < programs.length; i++) {
            programs[i] = new Program(names[random.nextInt(names.length)] + names[random.nextInt(names.length)]);
        }

        return programs;
    }
}
