public interface Scallable{
    Ellipse extendEllipseA(Ellipse ellipse);
    Ellipse extendEllipseB(Ellipse ellipse);
    Round extendRoundA(Round round);
    Square extendSquareA(Square square);
    Rectangle extendRectangleA(Rectangle rectangle);
    Rectangle extendRectangleB(Rectangle rectangle);
}