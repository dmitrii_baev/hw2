import java.util.Random;
public class Channel {
    private String name;
    private int number;

    // массив Programs
    private Program programs[];
    private int programsCount;

    public Channel(String name, int number) {
        this.name = name;
        this.number = number;
        this.programs = new Program[10];
        this.programsCount = 0;
    }

    // метод для того, чтобы добавить Program
    public void letProgramIn(Program program) {
        if (this.programsCount < programs.length) {
            this.programs[programsCount] = program;
            System.out.println("Added program" + program.getProgramName() + " to channel");
            programsCount++;
        } else {
            System.err.println("No more program space available");
        }
    }
    // Метод вывода случайной программы канала
    public String showPrograms(){
        String temp = this.getPrograms();
        System.out.println("Showing program: " + this.getPrograms() + " from the channel: " + this.getChannelName());
        return temp;
    }

    public String getPrograms() {
        Random random = new Random();
        return programs[random.nextInt(programs.length)].getProgramName();
    }

    public String getChannelName() {
        return name;
    }

    public int getChannelNumber() {
        return number;
    }
}