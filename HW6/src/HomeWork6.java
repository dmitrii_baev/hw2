public class HomeWork6 {
    public static void main(String[] var0) {
        // создаем ТВ
        Tv tv = new Tv("Samsung", 10);

        //создаем каналы
        Generator generator = new Generator();
        Channel channels[] = generator.generateC(10);
        Program programs[] = generator.generateP(20);
/*
        Channel channel1 = new Channel("A", 1);
        Channel channel2 = new Channel("B", 2);
        Channel channel3 = new Channel("C", 3);
        Channel channel4 = new Channel("D", 4);
        Channel channel5 = new Channel("E", 5);
        Channel channel6 = new Channel("F", 6);
        Channel channel7 = new Channel("G", 7);
        Channel channel8 = new Channel("H", 8);
        Channel channel9 = new Channel("I", 9);
        Channel channel10 = new Channel("J", 10);


        //создаем программы
        Program program1 = new Program("Good morning - 1");
        Program program2 = new Program("Good morning - 2");
        Program program3 = new Program("Good morning - 3");
        Program program4 = new Program("Good morning - 4");
        Program program5 = new Program("Good morning - 5");
        Program program6 = new Program("Good morning - 6");
        Program program7 = new Program("Good morning - 7");
        Program program8 = new Program("Good morning - 8");
        Program program9 = new Program("Good morning - 9");
        Program program10 = new Program("Good morning - 10");
        Program program11 = new Program("Good morning - 11");
*/

        //Добавляем каналы в ТВ
        for (int i = 0; i < channels.length; i++) {
            tv.letChannelIn(channels[i]);
        }

        //присваиваем программы каналам
        for (int i = 0; i < channels.length; i++) {
            channels[i].letProgramIn(programs[i * 2]);
            channels[i].letProgramIn(programs[(i * 2) + 1]);
        }
        /*
        channel1.letProgramIn(program1);
        channel1.letProgramIn(program2);
        channel2.letProgramIn(program3);
        channel2.letProgramIn(program4);
        channel3.letProgramIn(program5);
        channel3.letProgramIn(program6);
        channel4.letProgramIn(program7);
        channel4.letProgramIn(program8);
        channel5.letProgramIn(program9);
        channel5.letProgramIn(program10);
        channel6.letProgramIn(program10);
        channel6.letProgramIn(program1);
        channel7.letProgramIn(program2);
        channel7.letProgramIn(program3);
        channel8.letProgramIn(program4);
        channel8.letProgramIn(program5);
        channel9.letProgramIn(program6);
        channel9.letProgramIn(program7);
        channel10.letProgramIn(program8);
        channel10.letProgramIn(program9);
        channel1.letProgramIn(program10);
        */
        channels[1].showPrograms();
        tv.showChannel(1);
        // создал Пульт
        RemoteController remoteController = new RemoteController("Пульт1");

        remoteController.toggleChannel(0, tv);

        //Присваиваю пульт ТВ
        tv.setRemoteController(remoteController);//ошибка тут
        remoteController.goToTv(tv);//и тут
        System.out.println("Everything is done");
    }
}
