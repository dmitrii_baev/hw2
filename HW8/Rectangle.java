public class Rectangle extends GeometryFigure {
    //Constructor
    public Rectangle(String name, double centerX, double centerY, double lengthA, double lengthB) {
        if (lengthA > 0) {
            this.lengthA = lengthA;
        }
        if (lengthB > 0) {
            this.lengthB = lengthB;
        }
        this.centerX = centerX;
        this.centerY = centerY;
        this.name = name;
    }

    //Methods
    public double calculatePerimeter(){
        double perimeter = (this.lengthA * 2) + (this.lengthB * 2);
        return perimeter;
    }
    public double calculateSquare(){
        double square = this.lengthA * this.lengthB;
        return square;
    }
}