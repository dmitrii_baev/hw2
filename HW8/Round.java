public class Round extends Ellipse {
    //Constructor
    public Round(String name, double centerX, double centerY, double lengthA, double lengthB) {
        super(name, centerX, centerY, lengthA, lengthB);
        this.lengthB = lengthA;
    }
}