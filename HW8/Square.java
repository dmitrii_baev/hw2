public class Square extends Rectangle{
    //Constructor
    public Square(String name, double centerX, double centerY, double lengthA, double lengthB) {
        super(name, centerX, centerY, lengthA, lengthB);
        this.lengthB = lengthA;
    }
}