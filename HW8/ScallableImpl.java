public class ScallableImpl implements Scallable {
    public static final double DEFAULT_MULTIPLIERA = 1.0;
    public static final double DEFAULT_MULTIPLIERB = 1.0;

    private double multiplierA = DEFAULT_MULTIPLIERA;
    private double multiplierB = DEFAULT_MULTIPLIERB;

    public ScallableImpl(double multiplierA, double multiplierB) {
        this.multiplierA = multiplierA;
        this.multiplierB = multiplierB;
    }

    public ScallableImpl(double multiplierA) {
        this.multiplierA = multiplierA;
        this.multiplierB = multiplierA;
    }

    @java.lang.Override
    public Ellipse extendEllipseA(Ellipse ellipse) {
        ellipse.setLengthA(ellipse.getLengthA() * multiplierA);
        return ellipse;
    }

    @java.lang.Override
    public Ellipse extendEllipseB(Ellipse ellipse) {
        ellipse.setLengthB(ellipse.getLengthB() * multiplierB);
        return ellipse;
    }

    @java.lang.Override
    public Round extendRoundA(Round round) {
        round.setLengthA(round.getLengthA() * multiplierA);
        round.setLengthB(round.getLengthB() * multiplierA);
        return round;
    }

    @java.lang.Override
    public Square extendSquareA(Square square) {
        square.setLengthA(square.getLengthA() * multiplierA);
        square.setLengthB(square.getLengthB() * multiplierA);
        return square;
    }

    @java.lang.Override
    public Rectangle extendRectangleA(Rectangle rectangle) {
        rectangle.setLengthA(rectangle.getLengthA() * multiplierA);
        return rectangle;
    }

    @java.lang.Override
    public Rectangle extendRectangleB(Rectangle rectangle) {
        rectangle.setLengthB(rectangle.getLengthB() * multiplierB);
        return rectangle;
    }
}
