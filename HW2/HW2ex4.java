import java.util.Scanner;
import java.util.Arrays;
class HW2ex4 {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];
		int maxValue=0;
		int maxPosition=0;
		int minValue=0;
		int minPosition=0;
		int temp=0;
		float arraySum = 0;
		for (int count=0; count<=(n-1); count++) {
			array[count]=scanner.nextInt();
			if (count==0){
				minValue=array[count];
				maxValue=array[count];
				maxPosition=count;
				minPosition=count;
			}else{
				if(maxValue<array[count]){
					maxValue=array[count];
					maxPosition=count;
				}
				if(minValue>array[count]){
					minValue=array[count];
					minPosition=count;
				}
			}
		}
		System.out.println(Arrays.toString(array));
		temp=minValue;
		array[minPosition]=maxValue;
		array[maxPosition]=temp;		
		System.out.println(Arrays.toString(array));
	}			
}
