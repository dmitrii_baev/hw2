// TV
public class Tv {
    // поля - модель, номер
    private String model;
    private int number;
    // ссылка на RemoteController
    private RemoteController remoteController;

    // массив Channels
    private Channel channels[];
    private int channelsCount;

    // конструктор - принимает только модель и номер
    public Tv(String model, int number) {
        this.model = model;
        this.number = number;
        this.channels = new Channel[10];
        this.channelsCount = 0;
    }

    public void setRemoteController(RemoteController remoteController) {
        this.remoteController = remoteController;
    }

    // метод для того, чтобы добавить Channel
    public void letChannelIn(Channel channel) {
        if (this.channelsCount < channels.length) {
            this.channels[channelsCount] = channel;
            System.out.println("Added channel" + channel.getChannelName() + " to TV");
            channelsCount++;
        } else {
            System.err.println("No more channels available");
        }
    }

    // Метод обращения к каналу по номеру
    public void showChannel(int i){
        System.out.println("Showing channel: " + this.getChannels(i).getChannelName() + " on the TV: " + this.getModel() + " " + this.getNumber());
        System.out.print("Also: ");
        this.getChannels(i).showPrograms();
        return;
    }

    //Геттеры
    public String getModel() {
        return model;
    }

    public int getNumber() {
        return number;
    }

    public Channel getChannels(int i) {
        return channels[i];
    }
}