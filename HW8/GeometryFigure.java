public abstract class GeometryFigure{
    //Constants
    public static final double DEFAULT_CENTER_X = 0;
    public static final double DEFAULT_CENTER_Y = 0;
    public static final double DEFAULT_LENGTH = 0;
    public static final String DEFAULT_NAME = "New geometry shape";

    //Parameters
    protected String name = DEFAULT_NAME;
    protected double centerX = DEFAULT_CENTER_X;
    protected double centerY = DEFAULT_CENTER_Y;
    protected double lengthA = DEFAULT_LENGTH;
    protected double lengthB = DEFAULT_LENGTH;

    /*
    //Constructor
    public GeometryFigure(String name, float centerX, float centerY, float lengthA) {
        if (lengthA > 0) {
            this.lengthA = lengthA;
        }
        this.centerX = centerX;
        this.centerY = centerY;
        this.name = name;
    }
     */


    //Getters
    public String getName() {
        return name;
    }

    public double getCenterX() {
        return centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public double getLengthA() {
        return lengthA;
    }

    public double getLengthB() {
        return lengthB;
    }


    //Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setCenterX(double centerX) {
        this.centerX = centerX;
    }

    public void setCenterY(double centerY) {
        this.centerY = centerY;
    }

    public void setLengthA(double lengthA) {
        this.lengthA = lengthA;
    }

    public void setLengthB(double lengthB) {
        this.lengthB = lengthB;
    }


    //Methods
    public abstract double calculatePerimeter();
    public abstract double calculateSquare();


}