public class Ellipse extends GeometryFigure {
    //Constructor
    public Ellipse(String name, double centerX, double centerY, double lengthA, double lengthB) {
        if (lengthA > 0) {
            this.lengthA = lengthA;
        }
        if (lengthB > 0) {
            this.lengthB = lengthB;
        }
        this.centerX = centerX;
        this.centerY = centerY;
        this.name = name;
    }

    //Methods
    public double calculatePerimeter(){
        double perimeter = 4.0 * (Math.PI * (this.lengthA * this.lengthB) + (this.lengthA - this.lengthB)) / (this.lengthA + this.lengthB);
        return perimeter;
    }
    public double calculateSquare(){
        double square = Math.PI * this.lengthA * this.lengthB;
        return square;
    }
}