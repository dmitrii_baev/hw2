public class MovableImpl implements Movable {
    public static final double DEFAULT_XOFFSET = 1.0;
    public static final double DEFAULT_YOFFSET = 1.0;

    private double xOffsetDefault = DEFAULT_XOFFSET;
    private double yOffsetDefault = DEFAULT_YOFFSET;

    public MovableImpl() {
    }


    @Override
    public Ellipse moveEllipseX(Ellipse ellipse, double x) {
        ellipse.setCenterX(ellipse.getCenterX() + x);
        return ellipse;
    }

    @Override
    public Ellipse moveEllipseY(Ellipse ellipse, double y) {
        ellipse.setCenterY(ellipse.getCenterY() + y);
        return ellipse;
    }

    @Override
    public Round moveRoundX(Round round, double x) {
        round.setCenterX(round.getCenterX() + x);
        return round;
    }

    @Override
    public Round moveRoundY(Round round, double y) {
        round.setCenterY(round.getCenterY() + y);
        return round;
    }

    @Override
    public Square moveSquareX(Square square, double x) {
        square.setCenterX(square.getCenterX() + x);
        return square;
    }

    @Override
    public Square moveSquareY(Square square, double y) {
        square.setCenterY(square.getCenterY() + y);
        return square;
    }

    @Override
    public Rectangle moveRectangleX(Rectangle rectangle, double x) {
        rectangle.setCenterX(rectangle.getCenterX() + x);
        return rectangle;
    }

    @Override
    public Rectangle moveRectangleY(Rectangle rectangle, double y) {
        rectangle.setCenterY(rectangle.getCenterY() + y);
        return rectangle;
    }
}
