import java.util.Scanner;

// http://study-java.ru/uroki-java/formatirovanie-chisel-i-texta-v-java/

class HW3ex2_v1 {

	// x^2
	public static double f(double x) {
		return x * x; //input your function here
	}

	public static double calcIntegral(double a, double b, int n) {
		double h = (b - a) / n;
		
		double integral = 0;

		for (int i = 1; i <= n; i += 1) {
			if ((i % 2) == 1){
				integral = integral + 4 * f(a + i * h);
			}
			else{
				integral = integral + 2 * f(a + i * h);
			}
		}	
		integral = integral * h / 3;
		return integral;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);		
		System.out.print("input startPoint: ");
		double a = scanner.nextDouble();
		System.out.print("inputEndpoint: ");
		double b = scanner.nextDouble();
		
		int steps[] = {10, 100, 1_000, 10_000, 100_000, 1_000_000};
		double eps[] = new double[steps.length];
		double ys[] = new double[steps.length];
		for (int i = 1; i < steps.length; i += 1) {
			System.out.println("Number of steps is" + steps[i] +". Integral is equal to: " + calcIntegral(a, b, steps[i]));
		}
		
	
	}
}
