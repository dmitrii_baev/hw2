public interface Movable {
    Ellipse moveEllipseX(Ellipse ellipse, double x);
    Ellipse moveEllipseY(Ellipse ellipse, double y);
    Round moveRoundX(Round round, double x);
    Round moveRoundY(Round round, double y);
    Square moveSquareX(Square square, double x);
    Square moveSquareY(Square square, double y);
    Rectangle moveRectangleX(Rectangle rectangle, double x);
    Rectangle moveRectangleY(Rectangle rectangle, double y);
}