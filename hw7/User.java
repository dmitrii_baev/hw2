public class User {
    public static final String DEFAULT_FIRST_NAME = "";
    public static final String DEFAULT_LAST_NAME = "";
    public static final int DEFAULT_AGE = 0;
    public static final boolean DEFFAULT_WORKING_STATUS = false;
    private String firstName = DEFAULT_FIRST_NAME;
    private String lastName = DEFAULT_LAST_NAME;
    private int age = DEFAULT_AGE;
    private boolean isWorker = DEFFAULT_WORKING_STATUS;

    //Реализация Builder через статический внутренний класс
    public static class Builder {
        //Обязательные параметры
        //Необязательные параметры со значениями по умолчанию
        public String firstName = DEFAULT_FIRST_NAME;
        public String lastName = DEFAULT_LAST_NAME;
        public int age = DEFAULT_AGE;
        public boolean isWorker = DEFFAULT_WORKING_STATUS;

        //Конструктор с обязательными параметрами

        //Методы с возвращающим типом Builder для необязательного параметра firstName, lastName, age, isWorker
        public Builder firstName(String name) {
            this.firstName = name;
            return this;
        }

        public Builder lasttName(String name) {
            this.lastName = name;
            return this;
        }

        public Builder age(int val) {
            age = val;
            return this;
        }

        public Builder isWorker(boolean statement) {
            isWorker = statement;
            return this;
        }


        //Метод с возвращающим типом Good для генерации объекта
        public User build() {
            return new User(this);
        }
    }

    public static Builder builder() {
        Builder builder = new Builder();
        return builder;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", isWorker=" + isWorker +
                '}';
    }

    private User(Builder builder) {
        firstName = builder.firstName;
        lastName = builder.lastName;
        age = builder.age;
        isWorker = builder.isWorker;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWorker(boolean worker) {
        isWorker = worker;
    }
}